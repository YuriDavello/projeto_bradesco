//
//  ContentView.swift
//  ProjetoFinal_Bradesco
//
//  Created by Evosystems on 30/07/21.
//

import SwiftUI
import CoreData

	struct ContentView: View {
		
		//instancia a ListViewModel em um state object
		@StateObject private var listVM = ListViewModel(nome: "", sigla: "")
		
		//deleta departamento por meio das funções criada
		func deleteDepartamento(at offsets: IndexSet) {
			offsets.forEach { index in
				let departamento = listVM.departamentos[index]
				listVM.delete(departamento)
			}
			
			listVM.getAllDepartamentos()
		}
		
		var body: some View {
		
		//criação dos text fields e da lista de departamentos
			VStack {
				
					TextField("departamento", text: $listVM.nome)
					TextField("sigla", text: $listVM.sigla)
						.textFieldStyle(RoundedBorderTextFieldStyle())
				   
				   Button("Save") {
						listVM.save()
						listVM.getAllDepartamentos()
					}
				
				
				List {
					ForEach(listVM.departamentos, id: \.id) { departamento in
						HStack{
						Text(departamento.nome)
						Spacer()
						Text(departamento.sigla)
						}
					}.onDelete(perform: deleteDepartamento)
				}
				
				Spacer()
			}.padding()
			.onAppear(perform: {
				listVM.getAllDepartamentos()
			})
			
		}
	}

	struct ContentView_Previews: PreviewProvider {
		static var previews: some View {
			ContentView()
		}
	}
