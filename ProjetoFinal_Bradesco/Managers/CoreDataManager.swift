//
//  CoreDataManager.swift
//  ProjetoFinal_Bradesco
//
//  Created by Evosystems on 30/07/21.
//

import Foundation
import CoreData

class CoreDataManager {
	
	//criando as variáveis "shared" e "viewContext" dos tipos NSPersistentContainer
	//e NSManagedObjectContext, respsectivamente
	let persistentContainer: NSPersistentContainer
	static let shared = CoreDataManager()
	
	var viewContext: NSManagedObjectContext{
		return persistentContainer.viewContext
	}
	
	//função que retorna o departamento pelo ID
	func getDepartamentoById(id: NSManagedObjectID) -> Departamento? {
		do {
		 return try viewContext.existingObject(with: id) as? Departamento
		} catch {
		return nil
		}
	}
	
	//função que deleta o departamento
	func deleteDepartamento(departamento: Departamento) {
		viewContext.delete(departamento)
		save()
	}
		
	//função que retorna todos os departamentos por meio de um NSFetchRequest
	func getAllDepartamentos() -> [Departamento] {
		
		let request: NSFetchRequest<Departamento> = Departamento.fetchRequest()
		
		do{
			return try viewContext.fetch(request)
		} catch {
			return []
		}
	}
	
	//função que salva departamento
	func save() {
		do {
		try viewContext.save()
		} catch {
			viewContext.rollback()
			print(error.localizedDescription)
		}
	}
	
	//inicialização do CoreData
	private init() {
		persistentContainer = NSPersistentContainer(name: "DepartamentoDataModel")
		persistentContainer.loadPersistentStores { (description, error) in
			if let error = error{
				fatalError("Core Data Store falhou! \(error.localizedDescription)")
			}
		}
	}
}
	
