//
//  ProjetoFinal_BradescoApp.swift
//  ProjetoFinal_Bradesco
//
//  Created by Evosystems on 30/07/21.
//

import SwiftUI

@main
struct DepartamentosCRUDApp: App {
	
	var body: some Scene {
		WindowGroup {
			ContentView()
		}
	}
}
