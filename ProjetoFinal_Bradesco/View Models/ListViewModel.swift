//
//  ListViewModel.swift
//  ProjetoFinal_Bradesco
//
//  Created by Evosystems on 30/07/21.
//

import Foundation
import CoreData

//classe para manipular a lista
class ListViewModel: ObservableObject{
	
	//variável para cada departamento ter ID único
	private static var DepartamentoId: Int{
		get{
			return UserDefaults.standard.integer(forKey: "id")
		}
		set{
			UserDefaults.standard.set(newValue, forKey: "id")
		}
	}
	
	var nome: String
	var sigla: String
	
	//variável para armazenar array do tipo DepartamentoViewModel
	@Published var departamentos: [DepartamentoViewModel] = []
	
	//inicialização
	init(nome: String, sigla: String) {
		ListViewModel.DepartamentoId = ListViewModel.getUniqueIdentifier()
		self.nome = nome
		self.sigla = sigla
	}
	
	//função usada para pegar todos os departamentos para passar pra lista
	func getAllDepartamentos() {
		departamentos = CoreDataManager.shared.getAllDepartamentos().map(DepartamentoViewModel.init)
	}
	
	//função para deletar um departamento, caso exista
	func delete(_ departamento: DepartamentoViewModel){
		let existingDepartamento = CoreDataManager.shared.getDepartamentoById(id: departamento.id)
		
		if let existingDepartamento = existingDepartamento {
			CoreDataManager.shared.deleteDepartamento(departamento: existingDepartamento)
		}
	}
	
	//função para salvar departamento
	func save() {
		
		let departamento = Departamento(context: CoreDataManager.shared.viewContext)
		departamento.nome = nome
		departamento.sigla = sigla
		
		CoreDataManager.shared.save()
	}
						
	//função para incrementar variável DepartamentoId
	private static func getUniqueIdentifier() -> Int {
		DepartamentoId += 1
		return DepartamentoId
	}
}

//struct DepartamentoViewModel
struct DepartamentoViewModel {
	
	let departamento: Departamento
	
	var id: NSManagedObjectID {
		return departamento.objectID
	}
	
	var nome: String {
		return departamento.nome ?? ""
	}
	
	var sigla: String {
		return departamento.sigla ?? ""
	}
	
}
